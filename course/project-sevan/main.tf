#Map → it consists of key and value
resource"aws_security_group""myFirstSG" {
  name=var.sgname
  #  description="My first SG"
  ingress {
    description="80 from anywhere"
    from_port=80
    to_port=80
    protocol="tcp"
    cidr_blocks=["0.0.0.0/0"]
  }
}
resource"aws_instance""web" {
  count = length(var.vmnames)
  ami=lookup(var.ami,var.regionname)
  instance_type=var.vmsize
  #availability_zone = "us-east-1b"
  vpc_security_group_ids=[aws_security_group.myFirstSG.id]
  tags={
    Name = var.vmnames[count.index]
  }
}
