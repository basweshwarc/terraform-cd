data "aws_availability_zones" "all-azs" {
  state = "available"
}

resource "aws_instance" "vms" {
  count = length(data.aws_availability_zones.all-azs.names)
  ami = "ami-064ff912f78e3e561"
  instance_type = "t2.micro"
  availability_zone = data.aws_availability_zones.all-azs.names[count.index]
}

resource "aws_subnet" "" {
  count = length(data.aws_availability_zones.all-azs.names)
  cidr_block = "10.0.${count.index+1}.0/24"
  vpc_id = ""
}



