
variable "sgname" {

}
variable "amiid" {
  default = "ami-0c02fb55956c7d316"
}
variable "vmsize" {
  default = "t2.micro"
}
variable "vmname" {
  default = "newvm"
}
variable "regionname" {
  default = "us-east-1"
}