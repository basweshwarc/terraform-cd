#Create a new project which creates a vm and a security group.
#Ensure that vm uses the new security group with inbound traffic allowed on port 80 from anywhere.
#Ensure to make use of interpolation.

resource"aws_security_group""myFirstSG" {
  name=var.sgname
  #  description="My first SG"
  ingress {
    description="80 from anywhere"
    from_port=80
    to_port=80
    protocol="tcp"
    cidr_blocks=["0.0.0.0/0"]
  }
}
resource"aws_instance""web" {
  ami=var.amiid
  instance_type=var.vmsize
  #availability_zone = "us-east-1b"
  vpc_security_group_ids=[aws_security_group.myFirstSG.id]
  tags={
    Name = var.vmname
  }
}
