# Create IAm user terrauser
resource "aws_iam_user" "terrauser" {
  name = "terrauser"
  path = "/system/"

  tags = {
    tag-key = "tag-value"
  }

}

# create IAm group terragroup
resource "aws_iam_group" "terragroup" {
  name = "terragroup"
  path = "/users/"
}

# Create ebs valume

data "aws_ebs_volumes" "my_ebs" {

  filter {
    name   = "size"
    values = ["20"]
  }
}

# Create new security greoup
  resource "aws_security_group" "my_group" {
    vpc_id = "vpc-0e2ceff15f275bfcd"
  }

