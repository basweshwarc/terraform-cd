#Provision an ec2 machine with an attached ebs volume using interpolation.
resource "aws_volume_attachment" "vol" {
  device_name = "/dev/sdh"
  instance_id = aws_instance.newvm.id
  volume_id   = aws_ebs_volume.newvol.id
}

resource "aws_instance" "newvm" {
  ami = "ami-0c02fb55956c7d316"
  instance_type = "t2.micro"
  availability_zone = "us-east-1a"
  tags = {
    Name = "newvm"
  }
}
resource "aws_ebs_volume" "newvol" {
  availability_zone = aws_instance.newvm.availability_zone
  size = 10
  tags = {
    Name = "newvm"
  }
}
