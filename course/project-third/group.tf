resource "aws_iam_user_group_membership" "mem" {
  groups = ["terragroupnew"]
  user   = "terrausernew"
  depends_on = [aws_iam_group.terragroupnew, aws_iam_user.terrausernew]
}

resource "aws_iam_group" "terragroupnew" {
  name = "terragroupnew"
}
resource "aws_iam_user" "terrausernew" {
  name = "terrausernew"
}
