resource "aws_iam_user_group_membership" "mem1" {
  groups = [aws_iam_group.terragroupnew1.name]
  user   = aws_iam_user.terrausernew1.name
  #  depends_on = [aws_iam_group.terragroupnew, aws_iam_user.terrausernew]
}

resource "aws_iam_group" "terragroupnew1" {
  name = "terragroupnew1"
}
resource "aws_iam_user" "terrausernew1" {
  name = "terrausernew1"
}
