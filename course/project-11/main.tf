#Add a alb(aws_lb)/classic load balancer(aws_elb) in front of both the machines on port 80,
#ensure to configure health check on port 80 every 10 seconds.

resource "aws_vpc" "playvpc" {
  tags = {
    Name = var.vpcname
  }
  cidr_block = var.vpccidr
}
resource "aws_internet_gateway" "playvpcig" {
  vpc_id = aws_vpc.playvpc.id # ${aws_vpc.playvpc.id}
  tags = {
    Name = var.igname
  }
}
resource "aws_default_route_table" "playvpc-rt" {
  default_route_table_id = aws_vpc.playvpc.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.playvpcig.id
  }
  tags = {
    Name = "default-rt-playvpc"
  }
}
resource "aws_subnet" "playvpc-subnets" {
  count = length(var.subnetnames)
  cidr_block = var.subnetcidrs[count.index]
  availability_zone = var.azs[count.index]
  vpc_id = aws_vpc.playvpc.id
  tags = {
    Name = var.subnetnames[count.index]
  }
}
resource "aws_route_table" "playvpc-pvt-rt" {
  vpc_id = aws_vpc.playvpc.id
  tags = {
    Name = "playvpc-pvt-RT"
  }
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.playvpc-nat.id
  }
}
resource "aws_route_table_association" "playvpc-pvt-rt-association" {
  subnet_id      = aws_subnet.playvpc-subnets[1].id
  route_table_id = aws_route_table.playvpc-pvt-rt.id
}
resource "aws_eip" "nat-eip" {
  tags = {
    Name = "nat-eip"
  }
}

resource "aws_nat_gateway" "playvpc-nat" {
  subnet_id = aws_subnet.playvpc-subnets[0].id
  allocation_id = aws_eip.nat-eip.id
  tags = {
    Name = "playvpc-nat"
  }
}
resource "aws_security_group" "playvpcsg" {
  vpc_id = "${aws_vpc.playvpc.id}"
  ingress {
    description      = "TLS from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_instance" "machines" {
  count = 2
  ami = var.ami
  instance_type = var.instype
  subnet_id = aws_subnet.playvpc-subnets[0].id
  associate_public_ip_address = true
  security_groups = [aws_security_group.playvpcsg.id]
  user_data = <<EOF
#!/bin/bash
yum update -y
yum install -y httpd
service httpd start
chkconfig httpd on
echo "<body bgcolor="#FFFF00"></body>" > /var/www/html/index.html
EOF
}

resource "aws_lb" "playvpc-lb" {
  name               = "playvpc-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.playvpcsg.id]
  subnets            = [aws_subnet.playvpc-subnets[0].id,aws_subnet.playvpc-subnets[1].id]

  tags = {
    Environment = "production"
    Name = "playvpc-alb"
  }
}
resource "aws_lb_target_group" "playtest-tg" {
  name     = "playvpc-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.playvpc.id
}
resource "aws_lb_target_group_attachment" "test" {
  count = 2
  target_group_arn = aws_lb_target_group.playtest-tg.arn
  target_id        = aws_instance.machines[count.index].id
  port             = 80
}
resource "aws_lb_listener" "app" {
  load_balancer_arn = aws_lb.playvpc-lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.playtest-tg.arn
  }
}
#ansible
data "aws_availability_zones" "all-azs" {
  state = "available"
}

resource "aws_instance" "vms" {
  count = length(data.aws_availability_zones.all-azs.names)
  ami = "ami-0c02fb55956c7d316"
  instance_type = "t2.micro"
  availability_zone = data.aws_availability_zones.all-azs.names[count.index]
  key_name = "apr7"
  tags = {
    Name = "VM-${count.index}"
  }
  connection {
    type     = "ssh"
    user     = "ec2-user"
    private_key = file("apr7.pem")
    host     = self.public_ip
  }
  provisioner "remote-exec" {
    inline = [
      "sudo yum update -y",
      "sudo yum install -y httpd",
      "sudo service httpd start"
    ]

  }
  provisioner "local-exec" {
    command = "echo ${self.public_ip} >> hosts"
  }
  provisioner "local-exec" {
    command = "ansible -i hosts ${self.public_ip} -b -u ec2-user --private-key apr7.pem -m user -a 'name=demouser uid=1011 state=present'"
  }

}
