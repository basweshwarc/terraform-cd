variable "vmnames" {
  type = list(string)
}

variable "sgname" {

}
variable "amiid" {
  default = "ami-0c02fb55956c7d316"
}
variable "vmsize" {
  default = "t2.micro"
}

variable "regionname" {
}

variable "ami" {
  type = map(string)
  default = {
    "us-east-1" = "ami-0c02fb55956c7d316",
    "us-east-2" = "ami-064ff912f78e3e561"
  }
}
