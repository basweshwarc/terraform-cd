#Create a vpc in us-east-1 with CIDR 10.0.0.0/16 with the name playvpc.
#Create an IG and attach the IG with VPC in main RT.

resource "aws_vpc" "playvpc" {
  tags = {
    Name = var.vpcname
  }
  cidr_block = var.vpccidr
}
resource "aws_internet_gateway" "playvpcig" {
  vpc_id = aws_vpc.playvpc.id # ${aws_vpc.playvpc.id}
  tags = {
    Name = var.igname
  }
}
resource "aws_default_route_table" "playvpc-rt" {
  default_route_table_id = aws_vpc.playvpc.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.playvpcig.id
  }
  tags = {
    Name = "default-rt-playvpc"
  }
}




