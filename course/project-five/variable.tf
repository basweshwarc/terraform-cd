
variable "user" {
  default = "day2group"
}
variable "group" {
  default = "day2group"
}

variable "mem" {
  default =  "aws_iam_user.day2user.name"
}