#Create a new project with aws providers which creates one user day2user and a group day2group.
#And ensure this newuser becomes member of this group.
#Make use variables.tf file to declare them and then use terraform.tfvars to assign values.
#Try if you can override terraform.tfvars using -var or -var-file (anything.tfvars) options.


resource "aws_iam_user_group_membership" "mem" {
  groups = [aws_iam_group.day2group.name]
  user   = aws_iam_user.day2user.name

}

resource "aws_iam_group" "day2group" {
  name = var.user
}
resource "aws_iam_user" "day2user" {
  name = var.group
}

