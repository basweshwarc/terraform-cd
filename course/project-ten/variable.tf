variable "regionname" {}
variable "vpcname" {}
variable "vpccidr" {}
variable "igname" {}
variable "subnetnames" {
  type = list(string)
}
variable "subnetcidrs" {
  type = list(string)
}
variable "ami" {}
variable "instype" {}