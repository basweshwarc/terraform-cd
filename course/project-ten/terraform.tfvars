regionname = "us-east-1"
vpcname = "playvpc"
vpccidr = "10.0.0.0/16"
igname = "playvpc-ig"
subnetcidrs = ["10.0.1.0/24","10.0.2.0/24"]
subnetnames = ["playvpc-sub-1","playvpc-sub-2"]
ami = "ami-0c02fb55956c7d316"
instype = "t2.micro"