#Create a vpc in us-east-1 with CIDR 10.0.0.0/16 with the name playvpc.
#Create an IG and attach the IG with VPC in main RT.

#Add two subnets to the VPC with CIDRs 10.0.1.0/24 and 10.0.2.0/24 respectively with the name playvpc-sub-1 and
#playvpc-sub-2 in us-east-1a and us-east-1b zone

resource "aws_vpc" "playvpc" {
  tags = {
    Name = var.vpcname
  }
  cidr_block = var.vpccidr
}
resource "aws_internet_gateway" "playvpcig" {
  vpc_id = aws_vpc.playvpc.id # ${aws_vpc.playvpc.id}
  tags = {
    Name = var.igname
  }
}
resource "aws_default_route_table" "playvpc-rt" {
  default_route_table_id = aws_vpc.playvpc.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.playvpcig.id
  }
  tags = {
    Name = "default-rt-playvpc"
  }
}
resource "aws_subnet" "playvpc-subnets" {
  count = length(var.subnetnames)
  cidr_block = var.subnetcidrs[count.index]
  vpc_id = aws_vpc.playvpc.id
  tags = {
    Name = var.subnetnames[count.index]
  }
}
resource "aws_route_table" "playvpc-pvt-rt" {
  vpc_id = aws_vpc.playvpc.id
  tags = {
    Name = "playvpc-pvt-RT"
  }
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.playvpc-nat.id
  }
}
resource "aws_route_table_association" "playvpc-pvt-rt-association" {
  subnet_id      = aws_subnet.playvpc-subnets[1].id
  route_table_id = aws_route_table.playvpc-pvt-rt.id
}
resource "aws_eip" "nat-eip" {
  tags = {
    Name = "nat-eip"
  }
}

resource "aws_nat_gateway" "playvpc-nat" {
  subnet_id = aws_subnet.playvpc-subnets[0].id
  allocation_id = aws_eip.nat-eip.id
  tags = {
    Name = "playvpc-nat"
  }
}

#Create two VM in public subnet with public ip using amazon-linux-2 ami,
#with a security group that allows traffic on port 80 inbound from everywhere and two instances runs with the following script

resource "aws_security_group" "playvpcsg" {
  vpc_id = "${aws_vpc.playvpc.id}"
  ingress {
    description      = "TLS from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_instance" "machines" {
  count                       = 2
  ami                         = var.ami
  instance_type               = var.instype
  subnet_id                   = aws_subnet.playvpc-subnets[0].id
  associate_public_ip_address = true
  security_groups             = [aws_security_group.playvpcsg.id]
  user_data                   = <<EOF
#!/bin/bash
yum update -y
yum install -y httpd
service httpd start
chkconfig httpd on
echo "<body bgcolor="#FFFF00"></body>" > /var/www/html/index.html
EOF
}