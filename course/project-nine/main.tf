#Create a vpc in us-east-1 with CIDR 10.0.0.0/16 with the name playvpc.
#Create an IG and attach the IG with VPC in main RT.

#Add two subnets to the VPC with CIDRs 10.0.1.0/24 and 10.0.2.0/24 respectively with the name playvpc-sub-1 and
#playvpc-sub-2 in us-east-1a and us-east-1b zone

resource "aws_vpc" "playvpc" {
  tags = {
    Name = var.vpcname
  }
  cidr_block = var.vpccidr
}
resource "aws_internet_gateway" "playvpcig" {
  vpc_id = aws_vpc.playvpc.id # ${aws_vpc.playvpc.id}
  tags = {
    Name = var.igname
  }
}
resource "aws_default_route_table" "playvpc-rt" {
  default_route_table_id = aws_vpc.playvpc.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.playvpcig.id
  }
  tags = {
    Name = "default-rt-playvpc"
  }
}

resource "aws_subnet" "playvpc-subnets" {
  count = length(var.subnetnames)
  cidr_block = var.subnetcidrs[count.index]
  vpc_id = aws_vpc.playvpc.id
  tags = {
    Name = var.subnetnames[count.index]
  }
}



